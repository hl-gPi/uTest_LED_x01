import time, RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)

p1 = GPIO.PWM(16, 1000)   # freq 1000 Hz
p2 = GPIO.PWM(18, 20)     # freq 50 Hz

p1.start(0)
p2.start(50)

while True:
    for dc in range (5, 101, 5):
      p1.changeDutyCycle(dc)
      p2.changeFrequency(dc)
      time.sleep(0.2);


