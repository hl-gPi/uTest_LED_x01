import time
import RPi.GPIO as GPIO

pin = 12
freq = 200

GPIO.setmode(GPIO.BOARD)
GPIO.setup(pin, GPIO.OUT)

pwm = GPIO.PWM(pin, freq)
pwm.start(0)

try:
  print('press Ctrl+C to close Programs.')
  while True:
    for dc in range(0, 20, 1):
        pwm.ChangeDutyCycle(dc)
        time.sleep(0.2)

    for dc in range(20, 0, -1):
        pwm.ChangeDutyCycle(dc)
        time.sleep(0.2)

except KeyboardInterrupt:
  print('close')

finally:
  pwm.stop()
  GPIO.cleanup()


