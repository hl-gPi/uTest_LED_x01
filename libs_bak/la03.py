import RPi.GPIO as GPIO
import time

# blinking function
def blink(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(1)
    return

def blink_frequency(pin, freq):
  p = GPIO.PWM(pin, freq) 
  p.start(1)
  time.sleep(1)
  p.stop()


p12 = 12
p16 = 16
p18 = 18


# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)

# set up GPIO output channel
GPIO.setup(p12, GPIO.OUT)
GPIO.setup(p16, GPIO.OUT)
GPIO.setup(p18, GPIO.OUT)


for i in range(5, 100, 5):
  print(i)
  blink_frequency(p12, i)
  blink_frequency(p16, i)
  blink_frequency(p18, i)




GPIO.cleanup()


