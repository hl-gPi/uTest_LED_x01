import RPi.GPIO as GPIO
import time

# blinking function
def blink(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(1)
    return

p12 = 12
p16 = 16
p18 = 18


# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)

# set up GPIO output channel, we set GPIO4 (pin 7) to OUTPUT
GPIO.setup(p18, GPIO.OUT)

# blink GPIO4 (pin 7) 50 times
for i in range(0,50):
    blink(p18)

GPIO.cleanup()


