
import time
import RPi.GPIO as GPIO

from flask import Flask, render_template
app = Flask(__name__)


@app.route("/")
def main():
    return render_template('index.html')


@app.route("/hello")
def hello():
    return "Hello World!"


@app.route("/members")
def members():
    return "Members"


@app.route("/members/<string:name>/")
def getMember(name):
    return name


@app.route("/leds_lighting/<int:pin>")
def getLED_Light(pin=12):
    freq = 200

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin, GPIO.OUT)

    pwm = GPIO.PWM(pin, freq)
    pwm.start(0)

    for dc in range(0, 20, 1):
        pwm.ChangeDutyCycle(dc)
        time.sleep(0.2)

    for dc in range(20, 0, -1):
        pwm.ChangeDutyCycle(dc)
        time.sleep(0.2)

    pwm.stop()
    GPIO.cleanup()
    return render_template('leds.html', LEDxPINx=str(pin))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
